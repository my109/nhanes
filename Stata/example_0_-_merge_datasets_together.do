/* Goal
We will learn how to merge NHANES datasets together */

set maxvar 10000

cd "~/Downloads/NHANES/Stata"

use "demographics_clean.dta", clear

* MERGING DATASETS TOGETHER
* Let's merge the demographics, mortality, and response datasets together.
merge 1:1 seqn seqn_new sddsrvyr using "./mortality_clean.dta"
keep if _merge == 3
drop _merge
merge 1:1 seqn seqn_new sddsrvyr using "./weights_clean.dta"
keep if _merge == 3
drop _merge
merge 1:1 seqn seqn_new sddsrvyr using "./response_clean.dta"
keep if _merge == 3

* Save the merged dataset
save "./nhanes_merged.dta", replace
