/* Goal 
We will learn how to 
1. merge datasets together
2. run a linear regression model that accounts for NHANES sampling design
3. run a cox proportional hazard model that accounts for NHANES sampling design
*/


/* Loading NHANES datasets
Make sure you set your working directory to be the folder containing the file for the NHANES data. */ 
cd "~/Downloads/NHANES/Stata"

use "demographics_clean.dta", clear


* MERGING DATASETS TOGETHER
* Let's merge the demographics, mortality, response, and weights datasets together.
merge 1:1 seqn seqn_new sddsrvyr using "./mortality_clean.dta"
keep if _merge == 3
drop _merge
merge 1:1 seqn seqn_new sddsrvyr using "./response_clean.dta"
keep if _merge == 3
drop _merge


/* Ensure that sex and race/ethnicity variables are categorical
We ensured that sex (riagendr) is a categorical variable.
We ensured that race/ethnicity (ridreth1) is a categorical variable with the reference group as Non-Hispanic Whites (3). */
recode riagendr (1=0 "male") (2=1 "female"), generate(newgender)
drop riagendr
rename newgender riagendr


/* Choose only variables that you will use 
Let's subset the NHANES dataset to include only variables that we will use and participants who have info on all selected variables.
We will create two subsets:  one for the linear model and another for the cox proportional model. Notice the different size of these two subsets.
Tip: Make your dataset as small as possible and as big as necessary or else you'll be waiting for results for quite some time!
*/

/* For a generalized linear model, we will have the outcome variable be BMI (bmxbmi) and the predictors as age (ridageyr), sex (riagendr), and race/ethnicity (ridreth1).

For the Cox proportional hazard model, we will have the outcome variable be mortality status (mortstat) and time to death or end of the study period (permth_int) and the predictors as age (ridageyr), sex (riagendr), and race/ethnicity (ridreth1).
*/
keep seqn seqn_new sddsrvyr sdmvpsu sdmvstra wtmec2yr bmxbmi ridageyr riagendr ridreth1 mortstat permth_int


* Account for NHANES sampling design
* Use svyset to account for sampling design in the GLM and the Cox proportional hazard model
svyset [pw=wtmec2yr], psu(sdmvpsu) strata(sdmvstra) singleunit(nested)


/* Linear regression model
Let's run a generalized regression model and another one accounting for NHANES sampling design.
BMI is the outcome while the predictors are age, sex, and race/ethnicity.
*/
* Run a generalized linear model
glm bmxbmi ridageyr riagendr ridreth1, family(gaussian) link(identity) robust

* Run a generalized linear model accounting for the NHANES sampling design
glm bmxbmi ridageyr riagendr ridreth1, family(gaussian) link(identity) vce(survey)


/* Cox proportional hazard model
Let's run a Cox proportional hazard model accounting for NHANES sampling design.
Mortality status and time to death or end of the study period are the outcome and the predictors are age, sex, and race/ethnicity.
*/
* Run a cox proportional hazard model
stset permth_int, failure(mortstat)
stcox ridageyr riagendr ridreth1

* Run a cox proportional hazard model accounting for NHANES sampling design
stset permth_int, failure(mortstat)
stcox ridageyr riagendr ridreth1


